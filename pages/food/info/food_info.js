const WxParse = require('../../../utils/wxParse/wxParse.js');

//获取应用实例
var app = getApp()
Page({
  data: {
      hidden: false,            //加载提示
      msg:'加载中...',
      foodId:'',
      foodBook :{},
      ingredientsArray:[],
      stepArray:[],
      errorMsg:''
  },
  //页面加载监听===在此处获取newss数据
  onLoad : function (options) {
    console.log('菜单===详细信息');
    console.log('菜单===options.foodId'+options.id);
    var that = this;
    that.setData({
         foodId:options.id,
         hidden: false
    })
    wx.request({
      url: 'https://mouthwar.com/mouthwar/api/food/book/getfoodbook/'+options.id,
      data: {
         id: that.data.foodId
      },
      header: {
          'Content-Type': 'application/json'
      },
      success: function(res) {
        var data = res.data;//接口返回的数据
        console.log("新闻详细数据=="+data);
        //赋值
        var code = data.code;
        if (code==100200) {
            var resData = data.data;
            var ingredients = JSON.parse(resData.ingredients); 
            var step = JSON.parse(resData.step); 
            //赋值
            that.setData({
              foodBook: resData,
              ingredientsArray: ingredients,
              stepArray: step
            })
            //html格式转wxml格式
            //WxParse.wxParse('html',onefoodBook.html,that);
            
            setTimeout(function () {
              that.setData({
                hidden: true
              });
            }, 300);
          };
      },
      fail:function(res){
        console.log("接口调用失败="+res);
      }
    })
  },
  onShareAppMessage: function (res) {
    var that = this;
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '快来看，我学了新菜',
      path: '/pages/food/info/food_info?id='+that.data.foodId,
      success: function(res) {
        // 转发成功
      },
      fail: function(res) {
        // 转发失败
      }
    }
  }
})