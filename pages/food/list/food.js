//news.js
var WxSearch = require('../../../utils/wxSearch/wxSearch.js')
//获取应用实例
var app = getApp()
Page({
  data: {
    motto: '菜谱',        //title
    msg:'加载中...',
    hidden: false,            //加载提示
    wxSearchData: {
      value:''
    },             //查询名词
    page: 1,                  //当前页码
    typeId: '',               //当前类型
    RecommendPic:[],          //轮播图
    newsArray:[]              //新闻
  },
  //事件处理函数，跳转到详情页,暂时弃用了，改用navigage标签
  bindViewTap: function(event) {
    var newsId = event.currentTarget.dataset.id;
    var newsinfoUrl = '../info/news_info?id='+newsId;
    wx.navigateTo({
      url: newsinfoUrl
    })
  },
  //页面加载监听，页面准备好后执行===在此处获取newss数据
  onLoad: function (options) {
    //console.log('news===加载数据--'+options.type);
    var self = this;
    this.getNews({page: self.data.page});

    var that = this;
    WxSearch.init(that,43,['宫爆鸡丁','鱼香肉丝','糖醋里脊','蛋','素']);
    WxSearch.initMindKeys(['weappdev.com','微信小程序开发','微信开发','微信小程序']);
  },
  //加载数据
  getNews:function(data){
      var that = this;
      that.setData({
        hidden: false
      });
      if (!data) data = {};
      if (!data.page) data.page = 1;
      if (data.page === 1) {
        that.setData({
          RecommendPic: [],
          newsArray:[] 
        });
      }
      if(data.isNew){
        that.setData({
          RecommendPic: [],
          newsArray:[] 
        });
      }
      //http://huimian.app.china.com/NewsServlet.do?processID=getNewsList&Page=1&PageSize=20&Type=1
      wx.request({
        //url: 'http://mouthwar.com/mouthwar/api/yiyuan/news/getnewsinfolist',
        url: 'https://mouthwar.com/mouthwar/api/food/book/getfoodbook/randomlist',
        data: {
           typeId:that.data.typeId,
           page: data.page ,
           pageSize: '20',
           name: that.data.wxSearchData.value
        },
        header: {
            'Content-Type': 'application/json'
        },
        success: function(res) {
          var data = res.data;//接口返回的数据
          //console.log("接口返回的数据="+data);
          var code = data.code;
          if (code==100200) {
            var resData = data.data;
            var newsArray = resData.list;
            //console.log("菜谱数据=="+newsArray);
            var RecommendPic = resData.recommendPic;
            //console.log("轮播图=="+RecommendPic);
            //赋值
            that.setData({
              RecommendPic:RecommendPic,
              newsArray: that.data.newsArray.concat(newsArray.map(function (item) {
                return item;
              }))
            })
            setTimeout(function () {
              that.setData({
                hidden: true
              });
            }, 300);
          };
          
        }
      })
  },
  //详情页的点击跳转
  turnToTag:function(event){
    console.log("newsjs="+event);
    app.globalData.tagJS.turnToTag(event);
  },
  //下拉刷新
  lower: function(e) {
    var self = this;
    self.setData({
      page: self.data.page + 1
    });
    this.getNews({page: self.data.page});  
  },

  //搜索框相关操作
  wxSearchFn: function(e){
    console.log("点击查询？="+e);
    var that = this
    WxSearch.wxSearchAddHisKey(that);

    //调用查询
    this.getNews({page: that.data.page,isNew:true});
    
  },
  wxSearchInput: function(e){
    var that = this
    WxSearch.wxSearchInput(e,that);
  },
  wxSerchFocus: function(e){
    var that = this
    WxSearch.wxSearchFocus(e,that);
  },
  wxSearchBlur: function(e){
    var that = this
    WxSearch.wxSearchBlur(e,that);
  },
  wxSearchKeyTap:function(e){
    var that = this
    WxSearch.wxSearchKeyTap(e,that);
  },
  wxSearchDeleteKey: function(e){
    var that = this
    WxSearch.wxSearchDeleteKey(e,that);
  },
  wxSearchDeleteAll: function(e){
    var that = this;
    WxSearch.wxSearchDeleteAll(that);
  },
  wxSearchTap: function(e){
    var that = this
    WxSearch.wxSearchHiddenPancel(that);
  },


  onShareAppMessage: function (res) {
    var that = this;
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    var pathurl = '/pages/food/list/food';
    var titleStr = '家庭食谱——居家必备';
    
    return {
      title: titleStr,
      path: pathurl,
      success: function(res) {
        // 转发成功
      },
      fail: function(res) {
        // 转发失败
      }
    }
  }

})
