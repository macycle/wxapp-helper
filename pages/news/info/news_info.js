const WxParse = require('../../../utils/wxParse/wxParse.js');

//获取应用实例
var app = getApp()
Page({
  data: {
      wxParseData:[],
      hidden: false,            //加载提示
      msg:'加载中...',
      NewsId:'',
      newsinfo :{},
      errorMsg:''
  },
  //页面加载监听===在此处获取newss数据
  onLoad : function (options) {
    console.log('news===详细信息');
    console.log('news===options.newsId'+options.id);
    //http://mouthwar.com/mouthwar/api/yiyuan/news/getnewsinfo?id=9339fc17c90e57a2a59ee18a91a1c3e5
    var that = this;
    that.setData({
         NewsId:options.id,
         hidden: false
    })
    wx.request({
      url: 'https://mouthwar.com/mouthwar/api/yiyuan/news/getnewsinfo',
      //url: 'http://mouthwar.com/mouthwar/api/yiyuan/news/getchannel',
      data: {
         id: that.data.NewsId
      },
      header: {
          'Content-Type': 'application/json'
      },
      success: function(res) {
        var data = res.data;//接口返回的数据
        console.log("新闻详细数据=="+data);
        //赋值
        var code = data.code;
        if (code==100200) {
            var resData = data.data;
            var newsData = resData.showapi_res_body.pagebean;
            var newsArray = newsData.contentlist;
            //console.log("新闻数据=="+newsArray);
            //赋值
            var oneNewsInfo;
            if(newsArray.length>=0){
              oneNewsInfo =  newsArray[0];
            }

            that.setData({
              newsinfo: oneNewsInfo,
              wxParseData: oneNewsInfo.html
            })
            //html格式转wxml格式
            WxParse.wxParse('html',oneNewsInfo.html,that);
            
            setTimeout(function () {
              that.setData({
                hidden: true
              });
            }, 300);
          };
      },
      fail:function(res){
        console.log("接口调用失败="+res);
      }
    })
  }
})
